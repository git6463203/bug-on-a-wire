import { Entity } from "./class.js";
import GAME from "./constants.js";

const spawningPole = (poleDistance, electricWires, poles) =>{
    // Spawn Obstacle on random wire
    const pole = new Entity(GAME.CANVAS_WIDTH - 100, 500, GAME.POLE.WIDTH, GAME.POLE.HEIGHT);
    
    // Initialize obstacle velocity
    pole.velocity.x = -GAME.POLE.VELOCITY;
    let spawnRandomElectricWire = Math.floor(Math.random() * 100) + 1;
    if(spawnRandomElectricWire % 2 == 0 && poleDistance){
        let randomNumber = Math.floor(Math.random() * 4) + 0;
        const electricWire = new Entity(GAME.CANVAS_WIDTH + 100, GAME.WIRE.POSITIONS[randomNumber]-10, poleDistance+160, GAME.WIRE.HEIGHT+10);
        electricWire.velocity.x = -GAME.POLE.VELOCITY;
        electricWires.push(electricWire)
    }
    
    // Add to obstacles list
    poles.push(pole);
}

const spawningObstacle = (obstacles) =>{
    // Spawn Obstacle on random wire
    let randomNumber = Math.floor(Math.random() * 4) + 0;
    const obstacle = new Entity(GAME.CANVAS_WIDTH + 100, GAME.WIRE.POSITIONS[randomNumber] - GAME.OBSTACLE.HEIGHT, GAME.OBSTACLE.WIDTH, GAME.OBSTACLE.HEIGHT);

    // Initialize obstacle velocity
    obstacle.velocity.x = -GAME.OBSTACLE.VELOCITY;

    // Add to obstacles list
    obstacles.push(obstacle);
} 

export { spawningPole, spawningObstacle };