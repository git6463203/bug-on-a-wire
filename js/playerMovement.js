import GAME from "./constants.js";

const playerMovement = (e, player, PLAYER_X_MOVEMENT) =>{
            // Up movement
            if (e.code === 'ArrowUp' || e.code === 'KeyW') {
                if(player.currentWire !== 0){
                    player.currentWire -= 1;
                    player.y -= GAME.WIRE.POSITIONS[1] - GAME.WIRE.POSITIONS[0];
                }
            }

            // Down movement
            if (e.code === 'ArrowDown' || e.code === 'KeyS') {
                if(player.currentWire !== 3){
                    player.currentWire += 1;
                    player.y += GAME.WIRE.POSITIONS[1] - GAME.WIRE.POSITIONS[0];
                }
            }

            // Jump action
            if (player.isOnWire && (e.code === 'Space')) {
                player.velocity.y = -GAME.PLAYER.JUMP_VELOCITY;
                player.isOnWire = false;
                player.jumpVelocity = 5;
                if((player.velocity.x + player.jumpVelocity + player.x + player.acceleration.x) >= (GAME.CANVAS_WIDTH - 400)){
                    player.jumpVelocity = 0;
                }
            }
            
            // X-axis forward/right movement
            if (e.code === 'ArrowRight' || e.code === 'KeyD') {
                if (player.x + player.acceleration.x < (GAME.CANVAS_WIDTH - 400)){
                    player.acceleration.x = 50;
                    PLAYER_X_MOVEMENT = true;
                }
            }

            // X-axis backward/left movement
            if (e.code === 'ArrowLeft' || e.code === 'KeyA') {
                if (player.x - player.acceleration.x > 400){
                    player.acceleration.x = -50;
                    PLAYER_X_MOVEMENT = true;
                }
            }
}

export default playerMovement;