import GAME from "./constants.js";
import { fill, fillText } from "./utils.js";
import { wireImg, playerImg, birdImg, poleImg } from "./imageLoader.js";
let sx = 80, bsx = 0;
let fpsCounter = Date.now();

const drawLogic = (ctx, poles, wire0, wire1, wire2, wire3, electricWires, obstacles, player, timer, newHighScore) => {
    // Draw background
    fill(ctx);
       
    // Draw poles
    poles.forEach(pole => pole.drawPole(ctx, poleImg));

    // Draw wire
    wire0.drawWire(ctx, wireImg);
    wire1.drawWire(ctx, wireImg);
    wire2.drawWire(ctx, wireImg);
    wire3.drawWire(ctx, wireImg);

    // Draw electric wires
    electricWires.forEach(electricWire => electricWire.draw(ctx, 'blue', null))


    // Sprite frame swaps
    var now = Date.now();
    if (now - fpsCounter > 1000 / 10) {
        fpsCounter = now;
        sx += 250; 
        bsx +=32;
        if(sx > (125*7)){
            sx = 0;
        }
        if(bsx > 96){
            bsx = 0;
        }
    }        

    // Draw obstacles
    obstacles.forEach(obstacle=>{
        obstacle.drawImg(ctx, bsx+0.25, GAME.OBSTACLE.SOURCE_Y, GAME.OBSTACLE.SOURCE_W, GAME.OBSTACLE.SOURCE_H,
            GAME.OBSTACLE.DESTINATION_W, GAME.OBSTACLE.DESTINATION_H, birdImg, 100);
    })
    
    // Draw player
    player.drawImg(ctx, sx, GAME.PLAYER.SOURCE_Y, GAME.PLAYER.SOURCE_W,
        GAME.PLAYER.SOURCE_H, GAME.PLAYER.DESTINATION_W, GAME.PLAYER.DESTINATION_H, playerImg, 50);

    // Draw Score and Timer
    fillText(ctx, 100, 100, 'black', 100, 'pixelFont', 550, timer);
    fillText(ctx, GAME.CANVAS_WIDTH-400, 100, 'black', 100, 'pixelFont', 550, `Score: ${player.score}`);

    // Draw "Game Over" messgae
    if(player.gameOver){
        let scoreHistory = JSON.parse(localStorage.getItem("score"))[0];
        fillText(ctx, GAME.CANVAS_WIDTH/2-550, GAME.CANVAS_HEIGHT/2-100, 'white', 250, 'pixelFont', 600, `GAME OVER`);
        fillText(ctx, GAME.CANVAS_WIDTH/2-680, GAME.CANVAS_HEIGHT/2+200, 'white', 250, 'pixelFont', 580, `Final Score: ${player.score}`);
        fillText(ctx, GAME.CANVAS_WIDTH/2-680, GAME.CANVAS_HEIGHT/2+500, 'white', 250, 'pixelFont', 580, `${newHighScore}High Score: ${scoreHistory.score}`);
        fillText(ctx, GAME.CANVAS_WIDTH/2-900, GAME.CANVAS_HEIGHT/2+800, 'white', 150, 'pixelFont', 550, `Press space or enter to play again!`);
    }
};

export { drawLogic }