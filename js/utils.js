import { backgroundImg } from "./imageLoader.js";

"use strict";

// Draws a filled rectangle in canvas
const fillRect = (ctx, x, y, w, h, color, img) => {
    ctx.save();
    ctx.fillStyle = color;
    ctx.fillRect(x, y, w, h);
    ctx.restore();
};


// Draw player object on canvas i.e sprite animation
const fillWire = (ctx, dx, dy, dw, dh, img) =>{
    ctx.save();
    ctx.drawImage(img, dx, dy, dw, dh);
    ctx.restore();
}

// Draw player object on canvas i.e sprite animation
const fillPole = (ctx, dx, dy, img) =>{
    ctx.save();
    ctx.drawImage(img, dx, dy-300, 400, ctx.canvas.height);
    ctx.restore();
}

// Draw object image on canvas i.e sprite animation, static image
const fillImg = (ctx, sx, sy, sw, sh, dx, dy, dw, dh, img, yAdjust) =>{
    ctx.save();
    ctx.drawImage(img, sx, sy, sw, sh, dx, dy-yAdjust, dw, dh );
    ctx.restore();
}

// Fills a canvas with a certain color
const fill = (ctx) => {
    ctx.save();
    ctx.drawImage(backgroundImg, 0, 0, ctx.canvas.width, ctx.canvas.height); 
    ctx.restore();
};

// Fills a canvas with text
const fillText = (ctx, x, y, color, fontsize, fontfamily, fontweight, text) =>{
    ctx.font = `${fontweight} ${fontsize}px ${fontfamily}`
    ctx.fillStyle = color;
    ctx.fillText(`${text}` , x, y);
}


export { fill, fillRect, fillText, fillImg, fillPole, fillWire };