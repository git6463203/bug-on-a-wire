const loadImage = (src, width, height) => {
    var img = new Image(width, height);
    img.src = src;
    return img;
}

const playerImg = loadImage("assets/sprites/bug.png");
const backgroundImg = loadImage("assets/bg/city-scape.png");
const birdImg = loadImage("assets/sprites/pigeon.png");
const poleImg = loadImage("assets/img/pole.png");
const wireImg = loadImage("assets/img/wire.png");

export { playerImg, backgroundImg, wireImg, birdImg, poleImg };