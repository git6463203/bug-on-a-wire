import { spawningObstacle, spawningPole } from "./spawners.js";
import { Rectangle, Player } from "./class.js";
import { drawLogic } from "./objectRenderer.js";
import GAME from "./constants.js";
import playerMovement from "./playerMovement.js";

// Wire objects
let wire0, wire1, wire2, wire3;

// obstacles array to store spawned obstacles and obstacle spawning timeout id to clear obstacle spawning timeout
let obstacles = [], spawnObstacleID;

// Pole array to store spawned poles and pole spawning timeout id to clear pole spawning timeout
let poles = [], spawnPoleID;

// Entity objects
let updateID;

// Player Properties
let player, PLAYER_X_MOVEMENT = false;

// Time Constraints
let seconds = 0, minutes = 0, hours = 0;
let secs, mins, gethours;
let timer = "00: 00: 00";
let prevsecond = 0;

// Score Constraints
let newHighScore = '';

// Electricwires array to store spawned electrice wires
let electricWires = []

// Declaration for important uses
let poleDistance;

// Timer for score update
const startTimer = () => {
    let presentsecond = new Date().getSeconds();
    if(prevsecond === 0){
        prevsecond = presentsecond;
    }
    if(prevsecond !== presentsecond){
        player.score += 5;
        seconds += 1;
        prevsecond = presentsecond;
        
        if(seconds < 0){
            seconds = 1;
        }
        if (seconds === 60) {
            seconds = 0;
            minutes = minutes + 1;
        }
        mins = minutes < 10 ? "0" + minutes + ": " : minutes + ": ";
        if (minutes === 60) {
            minutes = 0;
            hours = hours + 1;
        }
        gethours = hours < 10 ? "0" + hours + ": " : hours + ": ";
        secs = seconds < 10 ? "0" + seconds : seconds;
        timer = gethours + mins + secs;
    }
}

// Resets game parameters back to the start
const reset = () => {

    // Reset player
    player.gameOver = false;
    player.y = GAME.WIRE.POSITIONS[0] - GAME.PLAYER.SIZE;
    player.x = GAME.PLAYER.START_X;
    player.velocity.y = 0;
    player.score = 0;
    
    // Reset Time Parameters
    seconds = 0;
    minutes = 0;
    hours = 0;
    timer = "00: 00: 00";
    secs="00";
    mins="00: ";
    gethours= "00: ";

    newHighScore='';

    // Clear obstacles
    electricWires = [];
    obstacles = [];
    poles = [];

    // Start the updates again
    update();
    spawnObstacle();
    spawnPole();
};

// Try to resart the game
const tryResart = (e) => {
    if (e.code === 'Enter' || e.code === 'Space') {
        // Remove event listener for self
        document.removeEventListener('keydown', tryResart);
        // Reset game
        reset();
    }
};

// Stops game, "Game Over" 
const stop = () => {
    let scoreHistory = JSON.parse(localStorage.getItem("score"));
    const currentScore = {
        'timer':timer,
        'score':player.score
    }
    if(scoreHistory.length < 11){
        scoreHistory.push(currentScore);
    }
    for(let i=0; i < scoreHistory.length-1; i++){
        console.log(scoreHistory.length, currentScore.score)
        if(scoreHistory[i].score < currentScore.score){
            scoreHistory.pop();
            scoreHistory.push(currentScore);
            if(i===0){
                newHighScore = 'New ';                
            }
            break;
        }
    }
    
    scoreHistory.sort((obj1, obj2) => obj2.score - obj1.score);
    localStorage.setItem('score', JSON.stringify(scoreHistory));

    player.gameOver = true;
    clearTimeout(updateID);
    clearTimeout(spawnObstacleID);
    clearTimeout(spawnPoleID);

    // Add event listener for game restart
    document.addEventListener('keydown', tryResart);
};

const spawnPole = () =>{
    spawnPoleID = setTimeout(spawnPole, 3500)

    spawningPole(poleDistance, electricWires, poles);
}

// Spawns a moving obstacle off-screen every few seconds
const spawnObstacle = () => {
    let randomTimeOut = Math.floor(Math.random() * (1000 - 300 + 1)) + 300;
    spawnObstacleID = setTimeout(spawnObstacle, randomTimeOut)
    
    spawningObstacle(obstacles);
};

const update = () => {
    updateID = setTimeout(update, 1 / GAME.FPS);
    startTimer();

    // Update Obstacles
    obstacles.forEach(obstacle => {
        obstacle.updateObstacle();
        
        // Check collision and trigger "game over"
        if (Rectangle.areColliding(player, obstacle)) {
            stop();
        }
    });

    if(poles.length == 2 && !poleDistance){
        poleDistance = poles[1].x - poles[0].x;
    }
    // Update Poles
    poles.forEach(pole => {
        pole.updateObstacle();
    });

    electricWires.forEach(electricWire =>{
        electricWire.updateObstacle();
        if (Rectangle.areColliding(player, electricWire)) {
            stop();
        }
    })

    // Remove offscreen obstacles
    obstacles = obstacles.filter(obstacle => obstacle.x > -obstacle.w); 

    // Remove offscreen poles
    poles = poles.filter(pole => pole.x > -pole.w);

    // Remove offscreen poles
    electricWires = electricWires.filter(electricWire => electricWire.x > -electricWire.w);

    // Update player
    player.updatePlayer();    
    
    if (player.y > GAME.WIRE.POSITIONS[player.currentWire] - 20) {
        player.y = GAME.WIRE.POSITIONS[player.currentWire] - player.h;
        player.isOnWire = true;
        player.jumpVelocity = 0;
    }
    
    if (player.acceleration.x){
        player.acceleration.x = 0;
        player.velocity.x = 0;
        setTimeout(()=>{PLAYER_X_MOVEMENT = false;}, 500)
    }
    
    if(player.x !== GAME.PLAYER.START_X && !PLAYER_X_MOVEMENT ){
        player.x -= 1;
    }
};

const draw = (ctx) => {
    requestAnimationFrame(() => draw(ctx));
    drawLogic(ctx, poles, wire0, wire1, wire2, wire3, electricWires, obstacles, player, timer, newHighScore);
};

const runGame = () => {
    // Get canvas from DOM
    const canvas = document.querySelector("canvas");

    // Set resolution
    canvas.width = GAME.CANVAS_WIDTH;
    canvas.height = GAME.CANVAS_HEIGHT;

    // Get canvas context
    const ctx = canvas.getContext('2d');
    
    // Initialize Wire objects
    wire0 = new Rectangle(0, GAME.WIRE.POSITIONS[0], GAME.CANVAS_WIDTH, GAME.WIRE.HEIGHT);
    wire1 = new Rectangle(0, GAME.WIRE.POSITIONS[1], GAME.CANVAS_WIDTH, GAME.WIRE.HEIGHT);
    wire2 = new Rectangle(0, GAME.WIRE.POSITIONS[2], GAME.CANVAS_WIDTH, GAME.WIRE.HEIGHT);
    wire3 = new Rectangle(0, GAME.WIRE.POSITIONS[3], GAME.CANVAS_WIDTH, GAME.WIRE.HEIGHT);
    
    // Initialize Player Object
    player = new Player(GAME.PLAYER.START_X, GAME.WIRE.POSITIONS[0] - GAME.PLAYER.SIZE, GAME.PLAYER.SIZE, GAME.PLAYER.SIZE);
    player.acceleration.y = GAME.PLAYER.GRAVITY;
     
    // Player Movements
    document.addEventListener('keydown', e => {
        if(!player.gameOver){          
            playerMovement(e, player, PLAYER_X_MOVEMENT);  
        }
    });

    // Start loops
    update();
    draw(ctx);
    spawnObstacle();
    spawnPole();
};

window.onload = () => {
    runGame();
};