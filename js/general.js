let bgtrack;
function showCanvas(){
    if (!bgtrack){
        bgtrack = new Audio("assets/sounds/bg_soundtrack.mp3");
        bgtrack.loop = true;
        bgtrack.play();
    }
    if(!localStorage.getItem("score")){
        localStorage.setItem("score", JSON.stringify([]));
    }
    document.getElementById("menu").style.display = "none";
    document.getElementById("myCanvas").style.display = "block";    
}

document.addEventListener('keydown', e => {
    showCanvas();
})