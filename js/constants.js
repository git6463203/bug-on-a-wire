// Constants
const GAME = {
    // Canvas Properties
    CANVAS_WIDTH: 3840,
    CANVAS_HEIGHT: 2160,
    FPS: 60,

    // Colors
    COLORS: {
        WIRE: 'black',
        OBSTACLES: 'grey',
        PLAYER: 'green',
        POLE: 'brown',
    },

    // Wire Properties
    WIRE: {
        POSITIONS: [500, 800, 1100, 1400],
        HEIGHT: 20,
    },

    // Obstacle Properties
    OBSTACLE: {
        WIDTH: 80,
        HEIGHT: 80,
        VELOCITY: 4,
        SOURCE_Y: 2,
        SOURCE_W: 30,
        SOURCE_H: 48,
        DESTINATION_W: 170,
        DESTINATION_H: 290
    },

    // Pole Properties
    POLE: {
        WIDTH: 100,
        HEIGHT: 1700,
        VELOCITY: 4,
    },

    // Player Properties
    PLAYER: {
        START_X: 128,
        SIZE: 25,
        JUMP_VELOCITY: 25,
        GRAVITY: 1.2,
        SOURCE_Y: 40,
        SOURCE_W: 280,
        SOURCE_H: 100,
        DESTINATION_W: 150,
        DESTINATION_H: 80
    },
};

export default GAME;