# Introduction
A 2d game where a bug/insect run across wires, avoiding obstacles using basic up, down 
and jump movement. 

# Genre
2D, Endless Runner

# Concept and Features Designs/Gameplay Overview:
1. Player character/bug: Players control a bug character that is moving in forward direction along the positive x-axis
2. Wires: There are four wires that players can move along, random electric wire may spawn 
3. Controls:
    - up: using 'w' or 'arrow-up' keys, jumps to another wire above in y-axis direction, if at the edge does nothing
    - down: using 's' or 'arrow-down' keys, jumps to another wire below in y-axis direction, if at the edge does nothing
    - space: jumps in place avoiding the obstacle
4. Obstacles:
    1. Birds: 
        - Game over if collided with 
        - Spawns in random numbers at random location(number increase with difficulty)
        - Never spawn in same y-axis or horizontal placements(leaving some space for players to pass through)
    2. Electric wires:
        - "Game over" if not avoided 
        - Delayed death after 1sec

# Development
1. Main Menu:
    - Play button
    - Instructions for controls
    - Simple animation(additional)
2. In-game HUDs and UI
    - Score indicator, 0.5sec = 5points
    - Timer 
    - Level indicator(additional)
3. "Game Over" UI    
    - Final Score and timer
    - "Game Over" Overlay to indicate run is over

# TODO
1. Firstly, implement code for player object, wire objects and obstacle objects
2. Implement features:
    - obstacle spawn rate and moving velocity
    - collision logic for player and obstacle
    - jump mechanics for player
3. Main Menu with start button
4. Ingame HUD; timer and score